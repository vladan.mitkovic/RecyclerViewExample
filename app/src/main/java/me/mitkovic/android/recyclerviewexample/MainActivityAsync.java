package me.mitkovic.android.recyclerviewexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import me.mitkovic.android.recyclerviewexample.adapters.ArtistsAdapter;
import me.mitkovic.android.recyclerviewexample.remote.PlaylistFetcher;
import me.mitkovic.android.recyclerviewexample.utils.Utils;

public class MainActivityAsync extends AppCompatActivity {

    private String dataURL = "http://www.bbc.co.uk/radio1/playlist.json";

    private PlaylistFetcher mDataFetcherAsyncTask;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.artists_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mDataFetcherAsyncTask = new PlaylistFetcher(this);
        mDataFetcherAsyncTask.execute(dataURL);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mDataFetcherAsyncTask.cancel(true);
    }


    public void onPlaylistFetched(String jsonString) {
        if (jsonString == null) return;
        recyclerView.setAdapter(new ArtistsAdapter(Utils.getArtistsList(jsonString), R.layout.list_item_artist, MainActivityAsync.this));
    }

}