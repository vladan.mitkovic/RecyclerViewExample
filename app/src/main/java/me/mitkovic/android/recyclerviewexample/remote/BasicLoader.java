package me.mitkovic.android.recyclerviewexample.remote;

import android.content.AsyncTaskLoader;
import android.content.Context;

public class BasicLoader extends AsyncTaskLoader<String> {

    private String mReqestUrl;

    public BasicLoader(Context context, String reqestUrl) {
        super(context);

        mReqestUrl = reqestUrl;
    }

    @Override
    protected boolean onCancelLoad() {
        return super.onCancelLoad();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();
    }

    @Override
    public String loadInBackground() {
       return loadData();
    }

    private String loadData() {
        HttpHandler httpHandler = new HttpHandler();
        return httpHandler.loadFromNetwork(mReqestUrl);
    }

}