package me.mitkovic.android.recyclerviewexample.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import me.mitkovic.android.recyclerviewexample.DetailActivity;
import me.mitkovic.android.recyclerviewexample.MainActivity;
import me.mitkovic.android.recyclerviewexample.R;
import me.mitkovic.android.recyclerviewexample.model.Artist;

public class ArtistsAdapter extends RecyclerView.Adapter<ArtistsAdapter.ArtistViewHolder> implements MainActivity.ItemClickListener {

    private List<Artist> artists;
    private int rowLayout;
    private Context context;

    public ArtistsAdapter(List<Artist> artists, int rowLayout, Context context) {
        this.artists = artists;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public ArtistsAdapter.ArtistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ArtistViewHolder(view, this);
    }


    @Override
    public void onBindViewHolder(ArtistViewHolder holder, final int position) {
        holder.artistTitle.setText(artists.get(position).getTitle());
        holder.artistName.setText(artists.get(position).getArtist());
        holder.artistId.setText(artists.get(position).getArtistId());
    }

    @Override
    public int getItemCount() {
        return artists.size();
    }

    @Override
    public void onItemClick(View view, int position) {
        View titleView = view.findViewById(R.id.artistTitle);

        Bundle bundle = new Bundle();

        bundle.putString("artistTitle", artists.get(position).getTitle());
        bundle.putString("artistName", artists.get(position).getArtist());
        bundle.putString("artistId", artists.get(position).getArtistId());

        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra("bundle", bundle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            titleView.setTransitionName("title");
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, titleView, titleView.getTransitionName());
            ActivityCompat.startActivity(context, intent, options.toBundle());
        } else {
            context.startActivity(intent);
        }
    }

    public void clear() {
        artists.clear();
        notifyDataSetChanged();
    }

    public static class ArtistViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout artistsLayout;
        TextView artistTitle;
        TextView artistName;
        TextView artistId;
        MainActivity.ItemClickListener mItemClickListener;

        private ArtistViewHolder(View view, MainActivity.ItemClickListener itemClickListener) {
            super(view);
            artistsLayout = (LinearLayout) view.findViewById(R.id.artists_layout);
            artistTitle = (TextView) view.findViewById(R.id.artistTitle);
            artistName = (TextView) view.findViewById(R.id.artistName);
            artistId = (TextView) view.findViewById(R.id.artistId);
            mItemClickListener = itemClickListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }
}