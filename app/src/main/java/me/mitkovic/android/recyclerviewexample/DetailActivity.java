package me.mitkovic.android.recyclerviewexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.detail_activity));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle bundle = getIntent().getBundleExtra("bundle");

        String artistTitle = bundle.getString("artistTitle");
        String artistName = bundle.getString("artistName");
        String artistId = bundle.getString("artistId");

        TextView artistTitleTextView = (TextView) findViewById(R.id.artist_title_view);
        artistTitleTextView.setText(artistTitle);

        TextView artistNameTextView = (TextView) findViewById(R.id.artist_name_view);
        artistNameTextView.setText(artistName);

        TextView artistIdTextView = (TextView) findViewById(R.id.artist_id_view);
        artistIdTextView.setText(artistId);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}