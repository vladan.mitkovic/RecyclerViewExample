package me.mitkovic.android.recyclerviewexample.remote;

import java.io.InputStream;

public class HttpResponse {

    public final int mStatusCode;
    public final InputStream mInputStream;

    public HttpResponse(int statusCode, InputStream inputStream) {
        this.mStatusCode = statusCode;
        this.mInputStream = inputStream;
    }

    public InputStream getContent() {
        return mInputStream;
    }
}
