package me.mitkovic.android.recyclerviewexample;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import me.mitkovic.android.recyclerviewexample.adapters.ArtistsAdapter;
import me.mitkovic.android.recyclerviewexample.remote.BasicLoader;
import me.mitkovic.android.recyclerviewexample.utils.Utils;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    private static final int BASIC_LOADER_ID = 0;

    private String mDataURL = "http://www.bbc.co.uk/radio1/playlist.json";

    private RecyclerView mRecyclerView;
    private ArtistsAdapter mArtistsAdapter;

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.app_name));
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.artists_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        getLoaderManager().initLoader(BASIC_LOADER_ID, null, this);
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new BasicLoader(this, mDataURL);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        if (data == null) return;

        mArtistsAdapter = new ArtistsAdapter(Utils.getArtistsList(data), R.layout.list_item_artist, MainActivity.this);
        mRecyclerView.setAdapter(mArtistsAdapter);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        mArtistsAdapter.clear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        getLoaderManager().getLoader(BASIC_LOADER_ID).cancelLoad();
    }

//    public void onLoad(View v) {
//        getLoaderManager().getLoader(BASIC_LOADER_ID).forceLoad();
//    }

}