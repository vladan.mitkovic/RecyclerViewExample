package me.mitkovic.android.recyclerviewexample.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Artist implements Parcelable {

    private String title;
    private String artist;
    private String artist_id;

    public Artist() {}

    public Artist(Parcel in) {
        title = in.readString();
        artist = in.readString();
        artist_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeString(artist_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getArtistId() {
        return artist_id;
    }

    public void setArtistId(String artist_id) {
        this.artist_id = artist_id;
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Artist> CREATOR = new Parcelable.Creator<Artist>() {
        @Override
        public Artist createFromParcel(Parcel in) {
            return new Artist(in);
        }

        @Override
        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    @Override
    public String toString() {
        return "{" + "artist_id= " + artist_id + " -- " + "artist= " + artist + " -- " + "title= " + title + " }";
    }
}