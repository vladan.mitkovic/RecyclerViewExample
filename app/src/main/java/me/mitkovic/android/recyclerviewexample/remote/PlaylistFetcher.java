package me.mitkovic.android.recyclerviewexample.remote;

import android.os.AsyncTask;

import me.mitkovic.android.recyclerviewexample.MainActivityAsync;

public class PlaylistFetcher extends AsyncTask<String, Void, String> {

    private MainActivityAsync mActivity;

    public PlaylistFetcher(MainActivityAsync activity) {
        mActivity = activity;
    }

    @Override
    protected String doInBackground(String... param) {
        String jsonStr = null;
        if(!isCancelled()) {
            HttpHandler httpHandler = new HttpHandler();
            jsonStr = httpHandler.loadFromNetwork(param[0]);
        }
        return jsonStr;
    }

    @Override
    protected void onPostExecute(String jsonString) {
        super.onPostExecute(jsonString);
        if (mActivity != null) {
            mActivity.onPlaylistFetched(jsonString);
        }
    }
}