package me.mitkovic.android.recyclerviewexample.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.mitkovic.android.recyclerviewexample.model.Artist;

public class Utils {

    public static List<Artist> getArtistsList(String jsonString) {
        if (jsonString == null) return null;

        List<Artist> artistsList = new ArrayList<>();

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            JSONObject playlist = jsonObj.getJSONObject("playlist");
            JSONArray artists = playlist.getJSONArray("a");

            for (int i = 0; i < artists.length(); i++) {
                JSONObject artistsJSONObject = artists.getJSONObject(i);

                String artistId = artistsJSONObject.getString("artist_id");
                String title = artistsJSONObject.getString("title");
                String artistName = artistsJSONObject.getString("artist");

                Artist artist = new Artist();
                artist.setArtistId(artistId);
                artist.setTitle(title);
                artist.setArtist(artistName);

                artistsList.add(artist);
            }
        } catch (final JSONException e) {
            Log.e("GILE", "Json parsing error: " + e.getMessage());
        }

        return artistsList;
    }
}
